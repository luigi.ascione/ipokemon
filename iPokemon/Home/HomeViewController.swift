//
//  HomeViewController.swift
//  iPokemon
//
//  Created by Luigi Acione on 02/05/23.
//

import UIKit
import Alamofire

class HomeViewController: UIViewController {
    
    private var pokemonDetails: PokemonDetails?
    private var selectedPokemon: Pokemon?
    private var currentOffset: Int = 0
    private var pokemonList: [Pokemon] = []
    private var totalElements: Int = 0
    
    @IBOutlet weak var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        
        tableView?.dataSource = self
        tableView?.delegate = self
        
        getPokemon()
    }
    
    private func getPokemon() {
        let apiToContact = Constants.baseUrl + "/api/v2/pokemon?offset=\(currentOffset)&limit=20"
        AF.request(apiToContact, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil)
            .response{ resp in
                switch resp.result{
                case .success(let data):
                    do{
                        let response = try JSONDecoder().decode(PokemonList.self, from: data!)
                        self.pokemonList += response.results
                        self.totalElements = response.count
                        
                        self.tableView?.reloadData()
                    } catch {
                        print(error.localizedDescription)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
    }
    
    private func getPokemonDetails() {
        let apiToContact = Constants.baseUrl + "/api/v2/pokemon/\(selectedPokemon?.name ?? "")"
        AF.request(apiToContact, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil)
            .response{ resp in
                switch resp.result{
                case .success(let data):
                    do{
                        let jsonData = try JSONDecoder().decode(PokemonDetails.self, from: data!)
                        
                        self.pokemonDetails = jsonData
                        
                        self.tableView?.reloadData()
                        
                        self.performSegue(withIdentifier: "goToDetails", sender: self)
                        
                    } catch {
                        print(error.localizedDescription)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell", for: indexPath) as! PokemonTableViewCell
        guard pokemonList.indices.contains(indexPath.row) else { return cell }
        cell.config(name: pokemonList[indexPath.row].name)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPokemon = Pokemon(name: pokemonList[indexPath.row].name, url: pokemonList[indexPath.row].url)
        getPokemonDetails()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath == IndexPath(row: tableView.numberOfRows(inSection: 0)-5, section: 0) {
            guard totalElements > pokemonList.count else { return }
            self.currentOffset += 20
            getPokemon()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? DetailsViewController
        vc?.pokemonDetails = pokemonDetails
        vc?.pokemonName = selectedPokemon?.name
    }
}
