//
//  PokemonTableViewCell.swift
//  iPokemon
//
//  Created by Luigi Acione on 04/05/23.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pokemonNameLabel: UILabel?
    @IBOutlet weak var containerView: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView?.layer.cornerRadius = 17
        self.selectionStyle = UITableViewCell.SelectionStyle.none
    }
    
    public func config(name: String) {
        pokemonNameLabel?.text = name.capitalized
    }
}
