//
//  Model.swift
//  iPokemon
//
//  Created by Luigi Acione on 04/05/23.
//

import Foundation

struct PokemonList: Decodable {
    var count: Int
    var next: String?
    var previous: String?
    var results: [Pokemon]
    
    init(count: Int, next: String, previous: String, results: [Pokemon]) {
        self.count = count
        self.next = next
        self.previous = previous
        self.results = results
    }
}

struct Pokemon: Decodable {
    var name: String
    var url: String
}

struct PokemonDetails: Decodable {
    var stats: [Stats]
    var types: [Typologyes]
    var id: Int
    var sprites: Sprites
}

struct Stats: Decodable {
    var base_stat: Int
    var effort: Int
    var stat: Stat
}

struct Stat: Decodable {
    var name: String
}

struct Typologyes: Decodable {
    var type: Typology
}

struct Typology: Decodable {
    var name: String
}

struct Sprites: Decodable {
    var other: Other
}

struct Other: Decodable {
    var home: Home
}

struct Home: Decodable {
    var front_default: String
}
