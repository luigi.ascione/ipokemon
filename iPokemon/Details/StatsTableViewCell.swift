//
//  StatsTableViewCell.swift
//  iPokemon
//
//  Created by Luigi Acione on 03/05/23.
//


import UIKit

class StatsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var statsNameLabel: UILabel?
    @IBOutlet weak var effortLabel: UILabel?
    @IBOutlet weak var effortValueLabel: UILabel?
    @IBOutlet weak var statLabel: UILabel?
    @IBOutlet weak var statValueLabel: UILabel?
    @IBOutlet weak var containerView: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCell.SelectionStyle.none
        containerView?.layer.cornerRadius = 17
    }
    
    public func config(statsName: String, effort: String, stat: String) {
        statsNameLabel?.text = statsName.capitalized
        effortValueLabel?.text = effort
        statValueLabel?.text = stat
    }
}
