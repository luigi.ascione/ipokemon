//
//  TypeTableViewCell.swift
//  iPokemon
//
//  Created by Luigi Acione on 03/05/23.
//

import UIKit

class TypeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var typeLabel: UILabel?
    @IBOutlet weak var containerView: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCell.SelectionStyle.none
        containerView?.layer.cornerRadius = 17
    }
    
    public func config(name: String) {
        typeLabel?.text = name.capitalized
    }
}
