//
//  DetailsViewController.swift
//  iPokemon
//
//  Created by Luigi Acione on 03/05/23.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var imageView: UIImageView?
    
    public var pokemonName: String?
    public var pokemonDetails: PokemonDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.dataSource = self
        tableView?.delegate = self
        
        self.navigationItem.title = pokemonName?.capitalized
        self.navigationItem.largeTitleDisplayMode = .always
        
        setImage()
    }
    
    private func setImage() {
        guard let picture = URL(string: pokemonDetails?.sprites.other.home.front_default ?? "") else { return }
        let session = URLSession(configuration: .default)
        let downloadPicTask = session.dataTask(with: picture) { (data, response, error) in
            if let e = error {
                print("Error downloading picture: \(e)")
            } else {
                if let res = response as? HTTPURLResponse {
                    print("Downloaded picture with response code \(res.statusCode)")
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        DispatchQueue.main.async {
                            self.imageView?.image = image
                        }
                    } else {
                        print("Couldn't get image: Image is nil")
                    }
                } else {
                    print("Couldn't get response code for some reason")
                }
            }
        }
        downloadPicTask.resume()
    }
}

extension DetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Tipo"
        case 1:
            return "Statistiche"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return pokemonDetails?.types.count ?? 0
        case 1:
            return pokemonDetails?.stats.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "typeCell", for: indexPath) as! TypeTableViewCell
            guard let pokemonDetails = pokemonDetails else { return cell }
            guard pokemonDetails.types.indices.contains(indexPath.row) else { return cell }
            cell.config(name: pokemonDetails.types[indexPath.row].type.name)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "statsCell", for: indexPath) as! StatsTableViewCell
            guard let pokemonDetails = pokemonDetails else { return cell }
            guard pokemonDetails.stats.indices.contains(indexPath.row) else { return cell }
            cell.config(statsName: pokemonDetails.stats[indexPath.row].stat.name, effort: String(pokemonDetails.stats[indexPath.row].effort), stat: String(pokemonDetails.stats[indexPath.row].base_stat))
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
