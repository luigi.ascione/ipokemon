//
//  Constants.swift
//  iPokemon
//
//  Created by Luigi Acione on 04/05/23.
//

import UIKit

struct Constants {
    static let baseUrl: String = "https://pokeapi.co"
}
